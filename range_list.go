package RangeList

import (
	"fmt"
	"strings"

	"gitlab.com/lyqscmy/RangeList/interval"
)

// RangeList represents a set of possibly disjointed Ranges.
// All methods requiring a Range will return error if the provided range is inverted or empty.
// Internal using an interval tree to efficiently store and search for non-overlapping ranges.
//
// RangeList is not safe for concurrent use by multiple goroutines.
type RangeList struct {
	t *interval.IntTree
}

func NewRangeList() *RangeList {
	return &RangeList{t: interval.NewIntTree()}
}

// Add will attempt to add the provided Range to the RangeList
// It first uses the interval tree to look up the current ranges which overlap with the new range.
// If there is no overlap, the new range will be added and return nil.
// If there is some overlap, the function will attempt to merge any ranges that overlap.
func (rangeList *RangeList) Add(rangeElement [2]int) error {
	if rangeElement[0] == rangeElement[1] {
		return nil
	}
	r := &IntInterval{Start: rangeElement[0], End: rangeElement[1]}
	overlaps := rangeList.t.Get(r)
	if len(overlaps) == 0 {
		if err := rangeList.t.Insert(r, false); err != nil {
			return err
		}
		return nil
	}
	first := overlaps[0].(*IntInterval)

	// If a current range fully contains the new range, no
	// need to add it.
	if first.Contains(r) {
		return nil
	}

	// Merge as many ranges as possible, and replace old range.
	first.Merge(r)
	for _, o := range overlaps[1:] {
		other := o.(*IntInterval)
		first.Merge(other)
		if err := rangeList.t.Delete(o, true); err != nil {
			return err
		}
	}
	rangeList.t.AdjustRanges()
	return nil
}

// Remove will attempt to remove the provided Range from the RangeList,
// It first uses the interval tree to look up the current ranges which overlap with the range to remove.
// For all ranges that overlap with the provided range, the overlapping segment of the range is removed.
// If the provided range fully contains a range in the RangeList,
// the range in the RangeList will be removed
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	if rangeElement[0] == rangeElement[1] {
		return nil
	}
	r := &IntInterval{Start: rangeElement[0], End: rangeElement[1]}
	overlaps := rangeList.t.Get(r)
	if len(overlaps) == 0 {
		return nil
	}

	for _, o := range overlaps {
		iv := o.(*IntInterval)

		delStart := iv.Start >= r.Start
		delEnd := iv.End <= (r.End)

		switch {
		case delStart && delEnd:
			// Remove the entire range.
			if err := rangeList.t.Delete(o, true /* fast */); err != nil {
				return err
			}
		case delStart:
			// Remove the start of the range by truncating.
			iv.Start = r.End
		case delEnd:
			// Remove the end of the range by truncating.
			iv.End = r.Start
		default:
			// Remove the middle of the range by splitting.
			oldEnd := iv.End
			iv.End = r.Start

			rSplit := &IntInterval{Start: r.End, End: oldEnd}
			if err := rangeList.t.Insert(rSplit, true /* fast */); err != nil {
				return err
			}
		}
	}
	rangeList.t.AdjustRanges()
	return nil
}

// Print returns a string representation of the ranges in the RangeList.
func (rangeList *RangeList) Print() string {
	var ss []string
	rangeList.ForEach(func(r interval.IntRange) error {
		ss = append(ss, fmt.Sprintf("[%d, %d)", r.Start, r.End))
		return nil
	})
	return strings.Join(ss, " ")
}

// IntInterval is a half-open interval
type IntInterval struct {
	Start, End int
	UID        uintptr
}

func (i *IntInterval) Overlap(b interval.IntRange) bool {
	// Half-open interval indexing.
	return i.End >= b.Start && i.Start <= b.End
}
func (i *IntInterval) ID() uintptr              { return i.UID }
func (i *IntInterval) Range() interval.IntRange { return interval.IntRange{Start: i.Start, End: i.End} }
func (i *IntInterval) String() string           { return fmt.Sprintf("[%d, %d)", i.Start, i.End) }

// Contains returns if the range in the out range fully contains the
// in range.
func (i *IntInterval) Contains(o *IntInterval) bool {
	return o.Start >= i.Start && i.End >= o.End
}

// Merge the provided ranges together into their union range. The
// ranges must overlap or the function will not produce the correct output.
func (i *IntInterval) Merge(o *IntInterval) {
	if o.Start < i.Start {
		i.Start = o.Start
	}
	if o.End > i.End {
		i.End = o.End
	}
}

// ForEach calls the provided function with each Range stored in
// the RangeList. An error is returned indicating whether the callback
// function saw an error, whereupon the Range iteration will halt
// (potentially prematurely) and the error will be returned from ForEach
// itself. If no error is returned from the callback, the method
// will visit all Ranges in the group before returning a nil error.
func (rangeList *RangeList) ForEach(f func(interval.IntRange) error) error {
	var err error
	rangeList.t.Do(func(i interval.IntInterface) bool {
		err = f(i.Range())
		return err != nil
	})
	return err
}
