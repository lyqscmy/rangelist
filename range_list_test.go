package RangeList

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRangeList(t *testing.T) {
	rl := NewRangeList()
	assert.Nil(t, rl.Add([2]int{1, 5}))
	assert.Equal(t, "[1, 5)", rl.Print())

	assert.Nil(t, rl.Add([2]int{10, 20}))
	assert.Equal(t, "[1, 5) [10, 20)", rl.Print())

	assert.Nil(t, rl.Add([2]int{20, 20}))
	assert.Equal(t, "[1, 5) [10, 20)", rl.Print())

	assert.Nil(t, rl.Add([2]int{20, 21}))
	assert.Equal(t, "[1, 5) [10, 21)", rl.Print())

	assert.Nil(t, rl.Add([2]int{2, 4}))
	assert.Equal(t, "[1, 5) [10, 21)", rl.Print())

	assert.Nil(t, rl.Add([2]int{3, 8}))
	assert.Equal(t, "[1, 8) [10, 21)", rl.Print())

	assert.Nil(t, rl.Remove([2]int{10, 10}))
	assert.Equal(t, "[1, 8) [10, 21)", rl.Print())

	assert.Nil(t, rl.Remove([2]int{10, 11}))
	assert.Equal(t, "[1, 8) [11, 21)", rl.Print())

	assert.Nil(t, rl.Remove([2]int{15, 17}))
	assert.Equal(t, "[1, 8) [11, 15) [17, 21)", rl.Print())

	assert.Nil(t, rl.Remove([2]int{3, 19}))
	assert.Equal(t, "[1, 3) [19, 21)", rl.Print())
}
